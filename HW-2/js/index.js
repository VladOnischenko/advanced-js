// try...catch - эта конструкция позволяет обрабатывать ошибки во время исполнения кода.

const books = [
	{ 
	  author: "Скотт Бэккер",
	  name: "Тьма, что приходит прежде",
	  price: 70 
	}, 
	{
	 author: "Скотт Бэккер",
	 name: "Воин-пророк",
	}, 
	{ 
	  name: "Тысячекратная мысль",
	  price: 70
	}, 
	{ 
	  author: "Скотт Бэккер",
	  name: "Нечестивый Консульт",
	  price: 70
	}, 
	{
	 author: "Дарья Донцова",
	 name: "Детектив на диете",
	 price: 40
	},
	{
	 author: "Дарья Донцова",
	 name: "Дед Снегур и Морозочка",
	}
];

const rootBlock = document.querySelector('#root');
const ul = document.createElement("ul");
rootBlock.insertAdjacentElement('afterbegin', ul);

function justFN(arr){
	arr.forEach(item => {
		if(Object.keys(item).includes('author') && Object.keys(item).includes('name') && Object.keys(item).includes('price')){
			const li = document.createElement("li");
			li.innerText = `Author: ${item.author}, Name: ${item.name}, Price: ${item.price}`;
			ul.append(li)
		}
		try {
			if(!Object.keys(item).includes('author')){
				console.log(item);
				throw new Error(`Invalid key: Author`)
			}else if(!Object.keys(item).includes('name')){
				console.log(item);
				throw new Error(`Invalid key: Name`)
			}else if(!Object.keys(item).includes('price')){
				console.log(item);
				throw new Error(`Invalid key: Price`)
			}
		}catch(err) {
			console.error(err.message);
		}
	})
}
justFN(books)
