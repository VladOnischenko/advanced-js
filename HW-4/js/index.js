//Это набор методов веб-разработки, которые позволяют веб-приложениям работать асинхронно — обрабатывать любые запросы к серверу в фоновом режиме.

const wrapper = document.getElementById('wrapper');
const url = 'https://ajax.test-danit.com/api/swapi/films';

const loader = `
<div class="loader">
<span>L</span>
<span>O</span>
<span>A</span>
<span>D</span>
<span>I</span>
<span>N</span>
<span>G</span>
</div>`;


const promise1 = fetch(url)
	.then(response => response.json())
	.then(data => data)

const promise2 = promise1.then((arrayData) => {
	arrayData.forEach(({ characters, openingCrawl, episodeId, name }) => {
		const itemBlock = document.createElement('ul')
		itemBlock.setAttribute('class', 'list')
		itemBlock.innerHTML = `
			<li><b>Title: </b> ${name}</li><br>
			<li><b>Episod number: </b> ${episodeId}</li><br>
			<li><b>Description: </b> ${openingCrawl}</li><br>
			<li><b>Characters: </b>${loader}</li><br>`
		wrapper.appendChild(itemBlock)
		
		let requests = characters.map( urlItem => fetch(urlItem))
		Promise.all(requests)
		.then(responses => responses.forEach(response => {
			response.json().then(data => {
				setTimeout(() => {
					const itemCharacter = document.createElement('li')
					const loading = document.querySelectorAll('.loader')
					itemCharacter.innerHTML = `${data.name}`
					loading.forEach((el) => {
						el.style.display = 'none'
					})
					itemBlock.appendChild(itemCharacter)
				},2000)
			})
		}))
	})
})
