const request =  ({url, data, options = {}}) =>{
  return fetch(url, {
    method: "GET",
    ...options,
    body: JSON.stringify(data)
  })
}

export {request}