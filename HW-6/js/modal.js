import {request} from "./request.js";
import {Card} from "./card.js";

class Modal extends Card{
  constructor(id, userId, name, email, title, body) {
    super(id, userId, name, email, title, body)

  }
  addNewPost(){
    this.button = this.createElement('button', ['addPost'], 'Add new post')
    this.container.insertAdjacentElement('beforebegin', this.button)
    this.button.addEventListener('click', () => {
      this.createModal()
    })
  }
  createModal(){
    this.modal = this.createElement('form', ['form'])
    this.modal.innerHTML = `
        <button id="btnDel" class="form__btn">Close</button>
        <label><b>Title:</b></label> 
            <input type="text">
        <label><b>Body:</b></label> 
            <textarea class="area"></textarea>
        <button type="submit">add post</button>
    `
    document.body.append(this.modal)
    this.modal.querySelector('.form__btn').addEventListener('click', () => {
      this.modal.remove();
    })
    this.modalHandler()
  }
  modalHandler(){
    this.modal.addEventListener('submit', event => {
      event.preventDefault()
      const inputTitle = this.modal.querySelector('input[type="text"]').value
      const bodyText = this.modal.querySelector('.area').value
      this.userId = this.container.firstElementChild.dataset.user
      this.id = this.container.firstElementChild.dataset.post
      this.name = this.container.firstElementChild.querySelector('h3').innerText
      this.email = this.container.firstElementChild.querySelector('a').innerText

        const newPost = new Card(++this.id, this.userId, this.name, this.email, inputTitle, bodyText)
        newPost.userCard()
        newPost.renderNewPost()
        this.modal.remove()
      request({url: `https://ajax.test-danit.com/api/json/posts`,
        options:{method: "POST"},
        data:{id: this.id, userId: this.userId, title: inputTitle, body: bodyText}
       })
    })
  }
}

export {Modal}
