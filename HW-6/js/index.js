import {Modal} from "./modal.js"
import {Card} from "./card.js";
import {request} from "./request.js";
import {Attachment} from "./attachment.js";

async function getPage(){
   const preloader = document.querySelector('#preloader')
   const responseUsers = await request({url:"https://ajax.test-danit.com/api/json/users"})
   const dataUsers = await responseUsers.json()
    dataUsers.reverse()
   const posts = dataUsers.map( async ({ id: userId, name, email}) => {
     const responsePosts = await request({url:`https://ajax.test-danit.com/api/json/posts?userId=${userId}`})
     const dataPosts = await responsePosts.json()
     const result = dataPosts.map( ({id, body, title}) => {
          return {id, userId, name, email, options: {body, title}}
     })
          result.reverse()
          return result
     })

   Promise.all(posts)
    .then((result) =>{
      result.forEach( userInfo => {
       for (const user of userInfo){
         const {id, name, userId, email, options: {body, title}} = user
         const card = new Card(id, userId ,name, email, title, body)
          card.renderUserCard()
       }
      })
      preloader.remove()
      const attachments = new Attachment()
      attachments.removeCard()
      const modal = new Modal()
      modal.addNewPost()
    })
}
getPage()
