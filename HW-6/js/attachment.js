import {request} from "./request.js";
import {Element} from "./element.js";

export class Attachment  extends Element{
   constructor() {
     super()
   }
  removeCard() {
    this.block = document.querySelector('.block')
    this.block.addEventListener('click', (event) => {
      const target = event.target
      if(target.tagName !== 'BUTTON') return
      const id = target.closest('.item').dataset.post
      request({
        url:`https://ajax.test-danit.com/api/json/posts/${id}`,
        options: { method: "DELETE" }
      })
      target.closest('.item').remove()
    })
  }
}
