export class Element{
  createElement(selector, classNames, text){
    const element = document.createElement(selector)
    if (text) {element.textContent = text}
    element.classList.add(...classNames)
    return element
  }
}