import {Attachment} from "./attachment.js";

export class Card extends Attachment{
  constructor(id, userId ,name, email, title, body) {
    super()
    this.id = id
    this.userId = userId
    this.name = name
    this.email = email
    this.title = title
    this.body = body
    this.container = document.querySelector('.block')
  }
  renderUserCard(){
    this.userCard()
    this.container.appendChild(this.item)
  }
  renderNewPost(){
    this.userCard()
    this.container.prepend(this.item)
  }
  userCard(){
    this.item = this.createElement('div', ['item'])
    this.item.setAttribute('data-user', `${this.userId}`)
    this.item.setAttribute('data-post', `${this.id}`)
    this.item.innerHTML = `
          <h3>${this.name}</h3> 
          <a href="mailto:${this.email}">${this.email}</a>
        <div >${this.title}</div>
        <p>${this.body}</p>
        <button id="btnDel" class="item__btn">X</button>
    `
  }
}

