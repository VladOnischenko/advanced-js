//Прототипное наследование - повторно использовать то, что есть у объекта , не копировать/переопределять его методы, а просто создать новый объект на его основе.

class Employee{
	constructor(name, age, salary){
		this._name = name
		this._age = age
		this._salary = salary
	}
	get name(){
		return this._name
	}
	get age(){
		return this._age
	}
	get salary(){
		return this._salary 
	}
	set salary(value){
		if(value >= 6500){
			this._salary = value
		}else {
			return alert(`Нельзя установить значение ниже 6500`)
		}
	}
	set name(value){
		this._name = value;
		return alert(`Ваше имя: ${this._name}`)
	}
	set age(value){
		if(value <= 0){
			return alert(`В параметрах есть ошибка`)
		}else {
			this._age = value
			return alert(`Ваш возраст: ${this._age}`)
		}
	}
}

const employee1 = new Employee('Vlad', 25, 15000)
const employee2 = new Employee('Vika', 15, 13560)
console.log('Info Employee 1', employee1);
console.log('Info Employee 2', employee2);


class Programmer extends Employee{

	constructor(name, age, salary, lang){
		super(name, age, salary)
		this._lang = lang;
	}
	get lang(){
		return this._lang
	}
	set salary(value){
		return super.salary = value
	}
	get salary(){
		return this._salary * 3
	}
}

const programmer1 = new Programmer('Oleg', 33, 23500, 'JavaScript');
const programmer2 = new Programmer('Maksim', 45, 27650, 'Python');
console.log('Info Programmer 1', programmer1);
console.log('Info Programmer 2', programmer2);

